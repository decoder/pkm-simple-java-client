# PKM simple java client

Very simple java client, with examples, for Decoder PKM. 
Includes a shell command to upload a full project in the PKM.

To generate the OpenApi stubs and compile:

```
mvn clean install
```

By default, the client connects to http://pkm-api_pkm_1:8080, as defined in https://gitlab.ow2.org/decoder/pkm-api/-/blob/master/documentation/README.md (with PKM and other tools launched using docker-compose).

These settings correspond to the dockerized PKM provided in https://gitlab.ow2.org/decoder/pkm-api (can be launched using "docker-compose up" - see documentation there).

Openapi yaml specification and SSL certificate (for the PKM quoted above) are available in src/main/resources.

## Sample code

Sample code is provided in eu.decoder.pkm.client.sample package.

See for example [PkmFileCRUD.java](src/main/java/eu/decoder/pkm/client/sample/PkmFileCRUD.java) (creates, reads and deletes a java source file in the PKM).

## Upload a project in the PKM

The eu.decoder.pkm.client.PkmProjectLoader class allows to upload a full project source code in the PKM.

For example, to upload a clone of MyThaiStar project (https://github.com/devonfw/my-thai-star):

Clone the MyThaiStar sample project (here, in /tmp):

```
$ cd /tmp
$ git clone https://github.com/devonfw/my-thai-star
```

Upload it in the PKM:

```
mvn exec:java -Dexec.mainClass="eu.decoder.pkm.client.PkmProjectLoader" -Dexec.args="src/main/resources/MyThaiStar.properties"
```

The properties file for MyThaiStar upload can be defined as follows (with MyThaiStar cloned in /tmp):

```
# Base directory (eg. after "git clone" of MyThaiStar)
baseDir: /tmp/my-thai-star

# Relative path to code (to load in the PKM).
# Multiple paths allowed, separated by colons or semicolons
# (for example, "src/main/java:src/test/java").
codePath: java/mtsj:mrchecker

# Take only .java files
extension: .java

# PKM location, project name and credentials
baseUrl: http://pkm-api_pkm_1:8080
#baseUrl: https://pkm-api_pkm_1:8080
#certificate: src/main/resources/pkm_docker.crt
project: OW2-MyThaiStar
user: admin
password: admin

# Overwrite files if already exist
overwrite: true

# Mock test: if true, project is deleted at logout (default false)
#mockTest: false
```

## Download a project from the PKM

The eu.decoder.pkm.client.PkmProjectDownloader class allows to download a full project source code from the PKM.

For example, to download a project called OW2-MyThaiStar in the PKM:

```
mvn exec:java -Dexec.mainClass="eu.decoder.pkm.client.PkmProjectDownloader" -Dexec.args="src/main/resources/MyThaiStarDownload.properties"
```

The properties file for MyThaiStar upload can be defined as follows (with MyThaiStar cloned in /tmp):

```
# Base directory (target for download)
baseDir: /tmp/my-thai-star

# PKM location, project name and credentials
baseUrl: http://pkm-api_pkm_1:8080
#baseUrl: https://pkm-api_pkm_1:8080
#certificate: src/main/resources/pkm_docker.crt
project: OW2-MyThaiStar
user: admin
password: admin
```

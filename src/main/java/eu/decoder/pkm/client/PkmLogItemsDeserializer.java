package eu.decoder.pkm.client;

import java.lang.reflect.Type;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * Gson deserializer for PKM Logs collection items
 * @author Pierre-Yves Gibello - OW2
 *
 */
public class PkmLogItemsDeserializer implements JsonDeserializer<PkmLogItems> {

	@Override
    public PkmLogItems deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		PkmLogItems items = new PkmLogItems(null, null);
		
		JsonObject obj = json.getAsJsonObject();
		String val = obj.get("tool").getAsString();
		items.setTool(val == null ? "unknown" : val);
		val = obj.get("nature of report").getAsString();
		if(val != null) items.setNatureOfReport(val);
		val = obj.get("start running time").getAsString();
		if(val != null) items.setStartRunningTime(val);
		val = obj.get("end running time").getAsString();
		if(val != null) items.setEndRunningTime(val);
		
		JsonArray array = obj.getAsJsonArray("messages");
		if(array != null) {
			for(JsonElement element : array) {
				items.addLog(element.toString());
			}
		}
		
		array = obj.getAsJsonArray("errors");
		if(array != null) {
			for(JsonElement element : array) {
				items.addError(element.toString());
			}
		}
		return items;
	}
}

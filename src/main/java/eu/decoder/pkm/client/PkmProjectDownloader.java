package eu.decoder.pkm.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;

import org.openapitools.client.ApiException;
import org.openapitools.client.model.PkmFile;

public class PkmProjectDownloader {

	static Properties config;
	
	/**
	 * Download project from PKM
	 * @param client The PKM client
	 * @param baseDir Base directory where to download project (null for temporary directory to create)
	 * @return Directory that contains download project
	 */
	public static String downloadProject(PkmClient client, String baseDir) throws IOException, ApiException {
		
		Path tmpDir;
		if(baseDir != null && baseDir.trim().length() > 0) {
			tmpDir = Paths.get(baseDir);
			if(! Files.exists(tmpDir)) Files.createDirectory(tmpDir);
			else if(! Files.isDirectory(tmpDir)) throw new IOException(tmpDir + " is not a valid directory");
		} else {
			tmpDir = Files.createTempDirectory("pkm").toAbsolutePath();
		}
		
		List<PkmFile> pkmFiles = client.getAllSourceFiles(true, null);
		if(pkmFiles != null) {
			// Copy files to temp dir
			for(PkmFile pkmFile : pkmFiles) {
				Path parent = Paths.get(pkmFile.getRelPath()).getParent();
				if(parent == null) {
					String message = "Ignoring file " + pkmFile.getRelPath() + ": parent path unknown";
					System.err.println(message);
				} else {
					String relFileDir = parent.toString().trim();
					// If relative path is absolute, simply make it relative by removing first /
					// By convention, we should have a path relative to the project root
					// (so "/dir/file" has the same meaning as "dir/file")
					while(relFileDir.startsWith(File.separator) && relFileDir.length() > 0) {
						relFileDir = relFileDir.substring(1);
					}
					if(relFileDir.length() > 0) {
						Files.createDirectories(Paths.get(tmpDir.toString(), relFileDir));
						String fileContent = pkmFile.getContent();

						if(fileContent == null) { // OpenApi version mismatch may cause read issues...
							throw new ApiException("JML injection failed due to null PKM file content: OpenApi version mismatch?");
						}
			
						Files.writeString(Paths.get(tmpDir.toString(), pkmFile.getRelPath()),
								fileContent);
					}
				}
			}
		}
		
		return tmpDir.toAbsolutePath().toString();
	}


	/**
	 * Main program: download a project from a PKM, according to a configuration properties file
	 * @param args Properties file
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		if(args.length <= 0) {
			// error
			System.err.println("Usage: PkmProjectDownloader <propertiesFile>");
			System.exit(1);
		} else if(args.length == 1) {
			PkmProjectDownloader.config = new Properties();
			PkmProjectDownloader.config.load(new FileInputStream(args[0]));
			
			String baseDir = getProperty("baseDir");
			
			String project = getProperty("project");
			if(project == null) project = getProperty("database"); // Backward compatibility with previous versions
			if(project == null) {
				// error
				System.err.println("Error: missing project property");
				System.exit(1);
			}

			String baseUrl = getProperty("baseUrl");
			if(baseUrl == null) baseUrl = "http://pkm-api_pkm_1:8080";
			
			System.out.println("Download project " + project + " from PKM at " + baseUrl);
			// Initialize PKM client
			PkmClient client = new PkmClient(baseUrl, project, getProperty("certificate"));
			client.login(getProperty("user"), getProperty("password"), true);

			String outputDir = PkmProjectDownloader.downloadProject(client, baseDir);
			System.out.println("Project download done at " + outputDir);

			client.logout();
		}
        
		System.exit(0);
	}

	/**
	 * Retrieves a configuration property
	 * @param property The property name
	 * @return The property value (trimmed)
	 */
	private static String getProperty(String property) {
		String result = PkmProjectDownloader.config.getProperty(property);
		return (result == null ? null : result.trim());
	}
}

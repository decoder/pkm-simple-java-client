package eu.decoder.pkm.client.sample;

import org.openapitools.client.ApiException;
import org.openapitools.client.model.PkmFile;

import eu.decoder.pkm.client.PkmClient;

/**
 * Sample CRUD of source file in PKM
 * @author Pierre-Yves Gibello - OW2
 *
 */
public class PkmFileCRUD {
	public static void main(String[] args) throws Exception {
        
		// Initialize PKM client for database "OW2"
		PkmClient client = new PkmClient("https://pkm-api_pkm_1:8080",
				"OW2", "src/main/resources/pkm_docker.crt");

		// Login to PKM (database is created if not exists)
		client.login("admin", "admin", true);

        try {
        	
        	String javaCode = "public class Hello {"
           		 + " public static void main(String args[]) {"
    			 + " System.out.println(\"Hello\");"
    			 + " } }";
        
        	// Upload Hello.java source file in PKM
        	if(client.postSourceFile("Hello.java", javaCode, true)) {
            
        		// Retrieve Hello.java source file from PKM
        		PkmFile file = client.getSourceFile("Hello.java");
        		System.out.println(file);
            
        		// Delete Hello.java source file from PKM
        		client.deleteSourceFile("Hello.java");
        	}

        } catch (ApiException e) {
            e.printStackTrace(System.err);
        }
        
        // Logout from PKM
        client.logout();
    }
}

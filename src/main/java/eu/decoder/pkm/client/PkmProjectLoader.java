package eu.decoder.pkm.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintStream;
import java.util.Properties;
import java.util.StringTokenizer;

import org.openapitools.client.ApiException;

public class PkmProjectLoader {

	static Properties config;
	
	/**
	 * Load project in PKM
	 * @param client The PKM client
	 * @param baseDir Base directory of project to load
	 * @param codePath Code path (colon-separated), default "src"
	 * @param extension File extension (null for all files)
	 * @param overwrite Overwrite files or not
	 */
	public static void loadProject(PkmClient client, String baseDir, String codePath, String extension, boolean overwrite) {
		PkmProjectLoader.loadProject(client, baseDir, codePath, extension, overwrite, null, null);
	}

	/**
	 * Load project in PKM
	 * @param client The PKM client
	 * @param baseDir Base directory of project to load
	 * @param codePath Code path (colon-separated), default "src"
	 * @param extension File extension (null for all files)
	 * @param overwrite Overwrite files or not
	 * @param log PrintStream for normal logging (eg. System.out), null for none
	 * @param errLog PrintStream for error logging (eg. System.err), null for none
	 */
	public static void loadProject(PkmClient client, String baseDir, String codePath, String extension, boolean overwrite, PrintStream log, PrintStream errLog) {
		if(codePath == null) codePath = "src";
		StringTokenizer st = new StringTokenizer(codePath, ":;");

		while(st.hasMoreTokens()) {
			String pathToAdd = st.nextToken();
			// Check if pathToAdd is relative to baseDir (or includes it)
			if(! pathToAdd.startsWith(baseDir)) {
				if(pathToAdd.startsWith(File.separator)) pathToAdd = pathToAdd.substring(1);
				pathToAdd = baseDir + (baseDir.endsWith(File.separator) ? "" : File.separator) + pathToAdd;
			}
			try {
				if(client.postSourceFileOrDirectory(new File(baseDir), new File(pathToAdd),
					extension, overwrite)) {
						if(log != null) log.println("Successful PKM upload of " + pathToAdd);
				} else {
					if(errLog != null) errLog.println("Error: PKM upload of " + pathToAdd + " failed!!");
				}
			} catch(ApiException e) {
				if(errLog != null) errLog.println("Error: PKM upload of " + pathToAdd + " failed!!");
			}
		}
	}

	/**
	 * Main program: upload a project in a PKM, according to a configuration properties file
	 * @param args Properties file
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		if(args.length <= 0) {
			// error
			System.err.println("Usage: PkmProjectLoader <propertiesFile>");
			System.exit(1);
		} else if(args.length == 1) {
			PkmProjectLoader.config = new Properties();
			PkmProjectLoader.config.load(new FileInputStream(args[0]));
			
			String baseDir = getProperty("baseDir");
			if(baseDir == null) {
				// error
				System.err.println("Error: missing baseDir property");
				System.exit(1);
			}
			
			String project = getProperty("project");
			if(project == null) project = getProperty("database"); // Backward compatibility with previous versions
			if(project == null) {
				// error
				System.err.println("Error: missing project property");
				System.exit(1);
			}

			String codePath = getProperty("codePath");

			String baseUrl = getProperty("baseUrl");
			if(baseUrl == null) baseUrl = "http://pkm-api_pkm_1:8080";
			
			System.out.println("Load project " + baseDir + " in PKM at " + baseUrl + " (project " + project + ")");
			// Initialize PKM client
			PkmClient client = new PkmClient(baseUrl, project, getProperty("certificate"));
			client.login(getProperty("user"), getProperty("password"), true);
			
			PkmProjectLoader.loadProject(client, baseDir, codePath,
				getProperty("extension"), "true".equalsIgnoreCase(getProperty("overwrite")),
				System.out, System.err);

			if("true".equalsIgnoreCase(getProperty("mockTest"))) {
				client.setProjectTransient();
			}
			client.logout();
		}
        
		System.exit(0);
	}

	/**
	 * Retrieves a configuration property
	 * @param property The property name
	 * @return The property value (trimmed)
	 */
	private static String getProperty(String property) {
		String result = PkmProjectLoader.config.getProperty(property);
		return (result == null ? null : result.trim());
	}
}

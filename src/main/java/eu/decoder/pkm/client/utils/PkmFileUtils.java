package eu.decoder.pkm.client.utils;

import java.io.File;
import java.io.IOException;

/**
 * File utilities for PKM api
 * @author Pierre-Yves Gibello - OW2
 *
 */
public class PkmFileUtils {

	/**
	 * Retrieves path to a file, relative to a root directory
	 * @param root Root directory
	 * @param file File (expected to be in a sub-directory of root)
	 * @return Path to file, relative to root. If root is null or file is not under root, file's canonical path is returned.
	 * @throws IOException
	 */
	public static String getRelPath(File root, File file) throws IOException {
		String result = file.getCanonicalPath();
		if(root != null) {
			if(! root.isDirectory()) throw new IOException("root path " + root.getPath() + " must be a directory");
			String basePath = root.getCanonicalPath();

			if(result.startsWith(basePath)) {
				result = result.substring(basePath.length());
				if(result.startsWith(File.separator)) result = result.substring(1);
			}
		}
		return result;
	}

}


package eu.decoder.pkm.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.openapitools.client.ApiClient;
import org.openapitools.client.ApiException;
import org.openapitools.client.api.CodeApi;
import org.openapitools.client.api.InvocationsApi;
import org.openapitools.client.api.LogApi;
import org.openapitools.client.api.ProjectApi;
import org.openapitools.client.api.UserApi;
import org.openapitools.client.model.PkmFile;
import org.openapitools.client.model.PkmLog;
import org.openapitools.client.model.PkmLoginInfo;
import org.openapitools.client.model.PkmProject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;

import eu.decoder.pkm.client.utils.PkmFileUtils;

/**
 * PKM simple java client
 * @author Pierre-Yves Gibello - OW2
 *
 */
public class PkmClient {

	ApiClient apiClient;
	CodeApi codeApi;
	LogApi logApi;
	InvocationsApi processEngineApi;

	// database and project names are supposed identical
	// (database is created/deleted upon project creation/deletion)
	String database = "java-test-project";
	String apiKey;
	boolean dbExists = false;
	boolean transientProject = false;
	String user = null;

	/**
	 * Creates a PKM client
	 * @param baseUrl PKM base URL
	 * @param database PKM database name
	 */
	public PkmClient(String baseUrl, String database) {
		this.apiClient = new ApiClient();
		this.apiClient.setBasePath(baseUrl);
		if(database != null && database.trim().length() > 0) this.database = database.trim();
	}	

	/**
	 * Creates a PKM client
	 * @param baseUrl PKM base URL
	 * @param database PKM database name
	 * @param caCertPath Path to a SSL certificate file
	 * @throws FileNotFoundException if certificate file not found
	 */
	public PkmClient(String baseUrl, String database, String caCertPath) throws FileNotFoundException {
		this(baseUrl, database);
		if(caCertPath != null) {
			FileInputStream in = null;
			try {
				in = new FileInputStream(caCertPath);
				this.apiClient.setSslCaCert(in);
			} finally {
				if(in != null) try { in.close(); } catch(Exception ignore) {}
			}
		}
	}

	/**
	 * Logs into the PKM, and when specified, creates DB if not exists
	 * @param user User name (null if none)
	 * @param password Password (null if none)
	 * @param createDbIfNotExists If true, create DB is it does not exist
	 * @return Login key
	 * @throws ApiException
	 */
	@SuppressWarnings("rawtypes")
	public String login(String user, String password, boolean createDbIfNotExists) throws ApiException {
		UserApi apiInstance = new UserApi(this.apiClient);
        PkmLoginInfo loginInfo = new PkmLoginInfo();
        if(user != null) loginInfo.setUserName(user);
        if(password != null) loginInfo.setUserPassword(password);

        Object result = apiInstance.login(loginInfo);
		this.apiKey = ((LinkedTreeMap)result).get("key").toString();
		this.user = user;

		if(createDbIfNotExists && ! this.dbExists) {
			createProjectIfNotExists(this.database);
			this.dbExists = true;
		}

        return this.apiKey;
	}

	/**
	 * Logs into the PKM
	 * @param user User name (null if none)
	 * @param password Password (null if none)
	 * @return Login key
	 * @throws ApiException
	 */
	public String login(String user, String password) throws ApiException {
		return login(user, password, false);
	}
	
	/**
	 * Logs into the PKM
	 * @param key The API key (out of a previous login)
	 * @param createDbIfNotExists If true, create DB is it does not exist
	 */
	public void login(String key, boolean createDbIfNotExists) {
		this.apiKey = key;
		if(createDbIfNotExists && ! this.dbExists) {
			createProjectIfNotExists(this.database);
			this.dbExists = true;
		}
		if(this.user == null) this.user = "default";
	}

	/**
	 * Retrieves this client's API key
	 * @return The API key
	 */
	public String getApiKey() {
		return this.apiKey;
	}
	
	/**
	 * Retrieves this client's raw OpenApi client
	 * @return The raw OpenApi client
	 */
	public ApiClient getApiClient() {
		return this.apiClient;
	}
	
	/**
	 * Retrieves current user
	 * @return The user logged in
	 */
	public String getUser() {
		return this.user;
	}

	/**
	 * Make project transient (delete at logout)
	 */
	public void setProjectTransient() {
		this.transientProject = true;
	}
	
	/**
	 * Logs out of the PKM, and deletes DB if transient
	 */
	public void logout() throws ApiException {
		if(this.apiKey != null) {
			if(this.transientProject) {
				deleteProjectIfExists(this.database);
				this.dbExists = false;
			}
			UserApi apiInstance = new UserApi(this.apiClient);
			try {
				apiInstance.logout(this.apiKey);
			} finally {
				this.apiKey = null;
				this.user = null;
			}
		}
	}

	/**
	 * Creates a project in the PKM, if not exists
	 * @param name The project name
	 */
	public void createProjectIfNotExists(String name) {
		if(name != null) {
			ProjectApi projectApi = new ProjectApi(this.apiClient);
			PkmProject project = new PkmProject();
			project.setName(name);
			try {
				projectApi.postProject(this.apiKey, project);
				//projectApi.postProject(project, this.apiKey);
			} catch(Exception ignore) {
			}
		}
	}
	
	/**
	 * Deletes a project in the PKM, if exists
	 * @param name The project name
	 */
	public void deleteProjectIfExists(String name) {
		if(name != null) {
			ProjectApi projectApi = new ProjectApi(this.apiClient);
			try {
				projectApi.deleteProject(name, this.apiKey);
			} catch(Exception ignore) {
			}
		}
	}
	
	/**
	 * Retrieves database name
	 * @return The database name
	 */
	public String getDatabase() {
		return this.database;
	}
	
	/**
	 * Sets database name
	 * @param database The database name
	 */
	public void setDatabase(String database) {
		this.database = database;
	}

	/**
	 * Retrieves raw code OpenApi client
	 * @return code OpenApi client
	 */
	public CodeApi getCodeApi() {
		if(this.codeApi == null) this.codeApi = new CodeApi(this.apiClient);
		return this.codeApi;
	}
	
	/**
	 * Retrieves raw log OpenApi client
	 * @return log OpenApi client
	 */
	public LogApi getLogApi() {
		if(this.logApi == null) this.logApi = new LogApi(this.apiClient);
		return this.logApi;
	}
	
	/**
	 * Retrieves raw process engine OpenApi client
	 * @return process engine OpenApi client
	 */
	public InvocationsApi getInvocationsApi() {
		if(this.processEngineApi == null) this.processEngineApi = new InvocationsApi(this.apiClient);
		return this.processEngineApi;
	}

	/**
	 * Stores a source code file in the PKM
	 * @param relPath Relative path to the file (eg. "my/package/Hello.java")
	 * @param content File content
	 * @param overwrite If true, overwrite existing version if any
	 * @return true if success, false otherwise
	 * @throws ApiException
	 */
	public boolean postSourceFile(String relPath, String content, boolean overwrite) throws ApiException {
		List<PkmFile> sourceCodes = new LinkedList<>();
        PkmFile file = new PkmFile();
        file.setRelPath(relPath);
        file.setFormat(PkmFile.FormatEnum.TEXT);
        file.setEncoding("utf-8");
        file.setContent(content);
        file.setType("Code"); // Convention for RawSourceCode records
        
        sourceCodes.add(file);
        boolean result = false;
        if(overwrite) {
        	getCodeApi().putRawSourceCodes(this.database, this.apiKey, sourceCodes);
        	//getCodeApi().putRawSourceCodes(this.database, sourceCodes, this.apiKey);

        	/* Workaround in case of putRawSourceCode() malfunction
        	try {
        		PkmFile exists = getCodeApi().getRawSourceCode(this.database, relPath, this.loginKey);
        		getCodeApi().putRawSourceCodes(this.database, this.loginKey, sourceCodes);
        	} catch(ApiException e) {
        		getCodeApi().postRawSourceCodes(this.database, this.loginKey, sourceCodes);
        	}*/
        	result = true;
        } else {
        	String created = getCodeApi().postRawSourceCodes(this.database, this.apiKey, sourceCodes);
        	//String created = getCodeApi().postRawSourceCodes(this.database, sourceCodes, this.apiKey);
        	result = (created != null && created.trim().startsWith("Created"));
        }
        return result;
	}

	/**
	 * Stores a source code file in the PKM
	 * @param root Root dir the file is relative to
	 * @param file File to store (the path stored will be relative to root)
	 * @param overwrite If true, overwrite existing version if any
	 * @return true if success, false otherwise
	 * @throws ApiException
	 */
	public boolean postSourceFile(File root, File file, boolean overwrite) throws ApiException {
		try {
			return postSourceFile(
				PkmFileUtils.getRelPath(root, file),
				Files.readString(file.toPath()), overwrite);
		} catch(IOException e) {
			throw new ApiException(e);
		}
	}

	/**
	 * 
	 * @param root Root dir the file or directory is relative to
	 * @param file File or directory to store (the path stored will be relative to root)
	 * @param ext File extension (null for all files)
	 * @param overwrite If true, overwrite existing version if any
	 * @return true if complete success, false otherwise
	 * @throws ApiException
	 */
	public boolean postSourceFileOrDirectory(File root, File file, String ext, boolean overwrite) throws ApiException {
		boolean result = true;
		if(! file.exists()) {
			throw new ApiException("File not found: " + file);
		} else if(file.isDirectory()) {
			for(File f : file.listFiles()) {
				if(! postSourceFileOrDirectory(root, f, ext, overwrite)) result = false;
			}
		} else if(ext == null || file.getName().endsWith(ext)) {
			return postSourceFile(root, file, overwrite);
		}
		return result;
	}
	
	/**
	 * Retrieves all source files in database
	 * @param withContent true to retrieve file content in the results, false otherwise
	 * @param extension Extension of files to retrieve (null for all files)
	 * @return List of all source files (with or without content)
	 * @throws ApiException
	 */
	public List<PkmFile> getAllSourceFiles(boolean withContent, String extension) throws ApiException {
		List<PkmFile> result = null, files = null;
		try {
			result = files = getCodeApi().getRawSourceCodes(this.database, this.apiKey, ! withContent, 0, 0);
			//result = files = getCodeApi().getRawSourceCodes(this.database, ! withContent, 0, 0, this.apiKey);
		} catch(ApiException e) {
			if(e.getMessage().toLowerCase().contains("not found")) result = null;
			else throw e;
		}
		if(result != null && extension != null && extension.trim().length() > 0) {
			extension = extension.trim();
			result = new LinkedList<PkmFile>();
			for(PkmFile file : files) {
				if(file.getRelPath().endsWith(extension)) result.add(file);
			}
		}
		return result;
	}

	/**
	 * Retrieves a source file from the PKM
	 * @param relPath Relative path to the file (eg. "my/package/Hello.java")
	 * @return The requested file
	 * @throws ApiException
	 */
	public PkmFile getSourceFile(String relPath) throws ApiException {
		return getCodeApi().getRawSourceCode(this.database, relPath, this.apiKey);
	}
	
	/**
	 * Deletes a source file from the PKM
	 * @param relPath Relative path to the file (eg. "my/package/Hello.java")
	 * @throws ApiException
	 */
	public void deleteSourceFile(String relPath) throws ApiException {
		getCodeApi().deleteRawSourceCode(this.database, relPath, this.apiKey);
	}

	/**
	 * Log multiple items in single log (messages and/or errors)
	 * @param items The items to log
	 * @throws ApiException
	 */
	public void logItems(PkmLogItems items) throws ApiException {
		Gson gson = new Gson();
		//System.out.println(gson.toJson(items));
		System.out.println(items.toJson());
		getLogApi().putLogs(this.database, this.apiKey, Arrays.asList(gson.fromJson(items.toJson(), PkmLog.class)));
		//getLogApi().putLogs(this.database, Arrays.asList(items), this.apiKey);
	}

	/**
	 * Log a message or an error
	 * @param message The message or error to log
	 * @param tool Name of tool that logs
	 * @param error true if error, false if simple log
	 * @throws ApiException
	 */
	private void log(String message, String tool, boolean error) throws ApiException {
		PkmLogItems item = new PkmLogItems(tool, (error ? "Error" : "Log"));
		if(error) item.addError(message);
		else item.addLog(message);
		logItems(item);
	}

	/**
	 * Log a message
	 * @param message The message to log
	 * @param tool Name of tool that logs
	 * @throws ApiException
	 */
	public void log(String message, String tool) throws ApiException {
		log(message, tool, false);
	}
	
	/**
	 * Log an error
	 * @param message The error to log
	 * @param tool Name of tool that logs
	 * @throws ApiException
	 */
	public void logError(String message, String tool) throws ApiException {
		log(message, tool, true);
	}

	/**
	 * Retrieves logs (possibly filtering by tool)
	 * @param tool Name of tool whose logs should be retrieved (null for all tools)
	 * @return The logs requested
	 * @throws ApiException
	 */
	public List<PkmLogItems> getLogs(String tool) throws ApiException {
		List<PkmLog> rawLogs = getLogApi().getLogs(this.database, this.apiKey, null, null);
		if(rawLogs == null || rawLogs.size() <= 0) {
			return null;
		} else {
			List<PkmLogItems> logs = new LinkedList<PkmLogItems>();
			for(Object raw : rawLogs) {
				//Gson gson = new Gson();
				Gson gson = new GsonBuilder().registerTypeAdapter(PkmLogItems.class, new PkmLogItemsDeserializer()).create();
				String json = gson.toJson(raw);
				//System.out.println(json);
				PkmLogItems item = gson.fromJson(json, PkmLogItems.class);
				if(tool == null || tool.length() <= 0 || tool.equals(item.getTool())) {
					logs.add(item);
				}
			}
			return logs;
		}
	}

	public static void main(String args[]) throws Exception {
		PkmClient client = new PkmClient("http://pkm-api_pkm_1:8080", "ow2_lutece", "src/main/resources/pkm_docker.crt");
		java.util.Properties credentials = new java.util.Properties();
		try {
			// Load credentials (*.secret is in .gitignore to avoid commit)
			credentials.load(new FileInputStream("credentials.secret"));
		} catch(Exception e) {
			credentials.setProperty("user", "admin");
			credentials.setProperty("password", "admin");
		}
		String key = client.login(credentials.getProperty("user"), credentials.getProperty("password"), true);
		System.out.println(key);
		//client = new PkmClient("http://pkm-api_pkm_1:8080", "test", "src/main/resources/pkm_docker.crt");
		//client.login(key, true);

		//client.setProjectTransient();
		client.log("Test log", "simple-java-api");
		client.log("Test error", "simple-java-api", true);
		
		PkmLogItems logs = new PkmLogItems(null, null);
		logs.addLog("message1");
		logs.addError("message2");
		//System.out.println(logs);
		client.logItems(logs);
		List<PkmLogItems> logList = client.getLogs(null);
		for(PkmLogItems log : logList) {
			System.out.println(log);
		}
		client.logout();
	}
}

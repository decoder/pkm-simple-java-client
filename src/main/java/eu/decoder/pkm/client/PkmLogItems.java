package eu.decoder.pkm.client;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

/**
 * Manage multiple logs to log in PKM Logs collection
 * @author Pierre-Yves Gibello - OW2
 *
 */
public class PkmLogItems {

	String tool = "unknown";
	boolean status = true;
	@SerializedName("start running time")
	String startRunningTime;
	@SerializedName("end running time")
	String endRunningTime;
	@SerializedName("nature of report")
	String natureOfReport = "Log";
	List<String> messages = new LinkedList<String>();
	List<String> errors = new LinkedList<String>();

	/**
	 * Creates a multiple items log
	 * @param tool The tool that logs
	 * @param natureOfReport The nature of report (generally "Log" or "Error": default "Log")
	 */
	public PkmLogItems(String tool, String natureOfReport) {
		if(tool != null) this.tool = tool;
		if(natureOfReport != null) this.natureOfReport = natureOfReport;
		this.startRunningTime = getTime();
		this.endRunningTime = getTime();
	}

	/**
	 * Adds a log message
	 * @param message The message to log
	 */
	public void addLog(String message) {
		messages.add(message);
	}
	
	/**
	 * Adds an error message
	 * @param message The error to log
	 */
	public void addError(String message) {
		errors.add(message);
	}
	
	/**
	 * Retrieves current time
	 * @return Current time
	 */
	private String getTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}

	public String getTool() {
		return tool;
	}
	public void setTool(String tool) {
		if(tool != null) this.tool = tool;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getStartRunningTime() {
		return startRunningTime;
	}
	public void setStartRunningTime(String startRunningTime) {
		if(startRunningTime != null) this.startRunningTime = startRunningTime;
	}
	public String getEndRunningTime() {
		return endRunningTime;
	}
	public void setEndRunningTime(String endRunningTime) {
		if(endRunningTime != null) this.endRunningTime = endRunningTime;
	}
	public String getNatureOfReport() {
		return natureOfReport;
	}
	public void setNatureOfReport(String natureOfReport) {
		if(natureOfReport != null) this.natureOfReport = natureOfReport;
	}
	public List<String> getMessages() {
		return messages;
	}
	public void setMessages(List<String> messages) {
		if(messages != null) this.messages = messages;
	}
	public List<String> getErrors() {
		return errors;
	}
	public void setErrors(List<String> errors) {
		if(errors != null) this.errors = errors;
	}

	public String toJson() {
		//Gson gson = new Gson();
		Gson gson = new GsonBuilder().registerTypeAdapter(PkmLogItems.class, new PkmLogItemsDeserializer()).create();
		return gson.toJson(this);
	}
	
	public String toString() { return toJson(); }
}


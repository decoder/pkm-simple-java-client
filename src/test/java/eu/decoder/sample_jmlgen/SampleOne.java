package eu.decoder.sample_jmlgen;

public class SampleOne {
	String anyString;
	int anyInt;
	int test;
	public int pubInt1, pubInt2;

	int getTest() {
		return test;
	}

	/**
	 * Create a new SampleOne
	 * @param anyString any String
	 * @param anyInt any integer
	 */
	public SampleOne(String anyString, int anyInt) {
		super();
		this.anyString = anyString;
		this.anyInt = anyInt;
	}

	/**
	 * Gets any string
	 * @return any String
	 */
	public String getAnyString() {
		return anyString;
	}

	/**
	 * Sets any String
	 * @param anyString any String
	 */
	public void setAnyString(String anyString) {
		this.anyString = anyString;
	}

	/**
	 * Gets any integer
	 * @return any integer
	 */
	public int getAnyInt() {
		return anyInt;
	}
	
	/**
	 * Sets public fields
	 */
	public void methodThatSetsPubFields() {
		pubInt1 = 1;
		pubInt2 = 2;
	}

	public void methodThatSetsPubFields(int p1, int p2) {
		pubInt1 = p1;
		pubInt2 = p2;
	}

	/**
	 * Sets any integer
	 * @param anyInt any integer
	 */
	public void setAnyInt(int anyInt) {
		this.anyInt = anyInt;
	}
	
	public SampleOne myself() {
		return this;
	}
}
